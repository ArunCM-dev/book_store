create database book_store;
use book_store;
create table books( id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, Name VARCHAR(500) NOT NULL, author VARCHAR(500) NOT NULL, website VARCHAR(1000) NOT NULL, state smallint NOT NULL DEFAULT 0, inserted_on DATETIME NOT NULL, updated_on DATETIME NOT NULL )Engine=InnoDB;

