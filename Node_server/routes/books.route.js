const express = require('express');
const bookRoutes = express.Router();
const connection = require('../dbconnection');

bookRoutes.post('/addbookdetails', function (req, res, next) {
    let query = `insert into books (name,author,website,inserted_on,updated_on) values ("${req.body.title}", "${req.body.author}","${req.body.website}",now(),now())`;
    connection.query(query, function (err, result) {
        if (err) {
            console.log(err);
            res.compressJson(JSON.stringify({ "st": 1, "sm": "Error in Adding Book Details" }));
        } else {
            res.compressJson(JSON.stringify({ "st": 0, "sm": "Book Details Added Successfully" }));
        }
    });
});


bookRoutes.post('/updatebookdetails', function (req, res, next) {
    let query = `UPDATE books SET name = "${req.body.name}",author = "${req.body.author}" , website = "${req.body.website}",updated_on = now() WHERE id="${req.body.id}" `;
    connection.query(query, function (err, result) {
        if (err) {
            console.log(err);
            res.compressJson(JSON.stringify({ "st": 1, "sm": "Error in Updating Book Details" }));
        } else {
            res.compressJson(JSON.stringify({ "st": 0, "sm": "Book Details Updated Successfully" }));
        }
    });
});

bookRoutes.post('/deletebookdetails', function (req, res, next) {

    let query = `DELETE FROM books WHERE id=${req.body.id}`;
    connection.query(query, function (err, result) {
        if (err) {
            console.log(err);
            res.compressJson(JSON.stringify({ "st": 1, "sm": "Error in Deleting Book Details" }));
        } else {
            res.compressJson(JSON.stringify({ "st": 0, "sm": "Book Details Deleted Successfully" }));
        }
    });
});

bookRoutes.post('/booklist', function (req, res, next) {
    let query = `SELECT id,name,author,website FROM books WHERE state='0'`;
    connection.query(query, function (err, result) {
        if (err) {
            console.log(err);
            res.compressJson(JSON.stringify({ "st": 1, "sm": "Error in Retriving book List", "data": [] }));
        } else {
            res.compressJson(JSON.stringify({ "st": 0, "sm": "Book List Retrived Successfully", "data": result }));
        }
    });
});

module.exports = bookRoutes;