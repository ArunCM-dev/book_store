const mysql = require('mysql');


config = {
    connectionLimit: 10,
    host: "localhost",
    user: "root",
    password: "Qwer123!#",
    database: "book_store",
    multipleStatements: true,
    dateStrings: true
};

var dbConnection = mysql.createPool(config);
handleDisconnect();

function handleDisconnect() {
    dbConnection.getConnection(function (err, connection) {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            }
            setTimeout(handleDisconnect, 10000);
        } else {
            console.log("Database connection successfull");
        }
    });
}

module.exports = dbConnection;
