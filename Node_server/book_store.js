const express = require("express");
const app = express();
const bodyparser = require('body-parser');
const cors = require('cors');
const jsonCompress = require("express-json-compress");


app.use(bodyparser.json({ limit: '50mb' }));
app.use(jsonCompress);
app.use(cors());

const books = require('./routes/books.route');
app.use('/books', books);


app.listen(4000, () => console.log(`Book store app listening on port !`))
