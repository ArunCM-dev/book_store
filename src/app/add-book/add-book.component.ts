import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BooksService } from '../services/books.service';
import { ViewBookComponent } from '../view-book/view-book.component';
@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  bookForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private bookService: BooksService) {
    this.bookForm = this.formBuilder.group({
      title: [{ value: '' }],
      author: [{ value: '' }],
      website: [{ value: '' }]
    });
  }

  ngOnInit(): void {
    this.resetData();
  }

  onSubmit() {
    this.submitted = true;
    if (this.bookForm.invalid) {
      console.log("error")
      return;
    } else {
      let formObj = this.bookForm.value;
      this.bookService.addCustomerDetails(formObj).subscribe((res: any) => {
        new ViewBookComponent(this.bookService).loadBookList();
        this.resetData();
      });
    }
  }
  resetData() {
    this.bookForm.reset();
  }
}
