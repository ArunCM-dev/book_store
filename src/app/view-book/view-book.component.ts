import { Component, OnInit, OnDestroy } from '@angular/core';
import { BooksService } from '../services/books.service';
import { takeWhile } from 'rxjs/operators';
import { LocalDataSource } from 'ng2-smart-table';
@Component({
  selector: 'app-view-book',
  templateUrl: './view-book.component.html',
  styleUrls: ['./view-book.component.css']
})
export class ViewBookComponent implements OnInit, OnDestroy {
  private alive = true;
  data = [];
  settings = {
    add: false,
    hideSubHeader: true,
    actions: {
      position: 'right',
    },
    edit: {
      editButtonContent: '<i class="bi bi-pencil success"></i>',
      saveButtonContent: '<i class="bi bi-check-lg"></i>',
      cancelButtonContent: '<i class="bi bi-x-lg"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="bi bi-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: '#',
        editable: false,
      },
      name: {
        title: 'Name'
      },
      author: {
        title: 'Author'
      },
      website: {
        title: 'Website'
      }
    },
    noDataMessage: "Nothing found"
  };
  source: LocalDataSource = new LocalDataSource();
  constructor(private bookService: BooksService) { }

  ngOnInit(): void {
    this.loadBookList();
  }

  loadBookList() {
    this.bookService.customerList({}).pipe(takeWhile(() => this.alive)).subscribe((res: any) => {
      if (res.st == 0) {
        this.source.load(res.data);
      } else {

      }
    });
  }

  onDeleteConfirm(event: any) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.bookService.deleteCustomerDetails({ "id": event.data.id }).subscribe((res: any) => {
        if (res.st == 0) {
          event.confirm.resolve();
          this.loadBookList();
        } else {

        }
      });
    }
  }

  onEditConfirm(event: any) {
    this.bookService.updateCustomerDetails(event.newData).subscribe((res: any) => {
      if (res.st == 0) {
        event.confirm.resolve();
        this.loadBookList();
      } else {
        
      }
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
