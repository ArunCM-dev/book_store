import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BooksService {
  url: String = `http://${environment.node_server_ip}:${environment.node_server_port}/books/`;
  constructor(private http: HttpClient) { }
  addCustomerDetails(reqParam:any){
    return this.http.post(this.url + 'addbookdetails', reqParam);
  }

  updateCustomerDetails(reqParam:any){
    return this.http.post(this.url + 'updatebookdetails', reqParam);
  }

  deleteCustomerDetails(reqParam:any){
    return this.http.post(this.url + 'deletebookdetails', reqParam);
  }
  
  customerList(reqParam:any){
    return this.http.post(this.url + 'booklist', reqParam);
  }
}
